// Ionic pointIn App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'pointIn' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'pointIn.controllers' is found in controllers.js
angular.module('pointIn', ['ionic', 'LocalStorageModule', 'pointIn.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.addLocation', {
      url: '/addLocation',
      views: {
        'menuContent': {
          templateUrl: 'templates/addLocations.html',
          controller: 'addLocationCtrl'
        }
      }
    })
    .state('app.pointInLists', {
      url: '/pointInLists',
      views: {
        'menuContent': {
          templateUrl: 'templates/pointInLists.html',
          //controller: 'PointInListsCtrl'
          controller: 'addLocationCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/pointInLists/:pointInDetailsId',
    views: {
      'menuContent': {
        templateUrl: 'templates/pointInDetails.html',
        controller: 'PointInListDetailsCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/pointInLists');
});
/*.config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('pointIn');
  });*/
