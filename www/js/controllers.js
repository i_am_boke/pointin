angular.module('pointIn.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})
.controller('addLocationCtrl', function($scope, $http, $state, $ionicModal, $ionicPopup, localStorageService){

/* ================================== add Location:: starts ======================================*/
    $scope.currentL = "currentL";
    $scope.searchL = "searchL";
    $scope.address = "";
    $scope.setPointInDesc = "";

    //initialize the pointInLists scope with empty array
    $scope.pointInLists = [];    
    //initialize the pointInObj scope with empty object
    $scope.pointInObj = {};    
    
    $scope.pointInCategories = [
      { title: 'Airport', id: 1, imgName: 'Airport'},
      { title: 'ATM', id: 2, imgName: 'ATM'},
      { title: 'Bar', id: 3, imgName: 'Bar'},
      { title: 'Building', id: 4, imgName: 'Building'},
      { title: 'Bustop', id: 5, imgName: 'Bustop'},
      { title: 'Cafe', id: 6, imgName: 'Cafe'},
      { title: 'Camp', id: 7, imgName: 'Camp'},
      { title: 'Casino', id: 8, imgName: 'Casino'},
      { title: 'Friend', id: 9, imgName: 'Friend'},
      { title: 'Food', id: 10, imgName: 'Food'},
      { title: 'Hill Station', id: 11, imgName: 'HillStation'},
      { title: 'Hotel', id: 12, imgName: 'Hotel'},
      { title: 'Hospital', id: 13, imgName: 'Hospital'},
      { title: 'Mall', id: 14, imgName: 'Mall'},
      { title: 'Parking', id: 15, imgName: 'Parking'},
      { title: 'Park', id: 16, imgName: 'Park'},
      { title: 'Person', id: 17, imgName: 'Person'},
      { title: 'Petrol Pump', id: 18, imgName: 'PetrolPump'},
      { title: 'Place', id: 19, imgName: 'Place'},
      { title: 'Pool', id: 20, imgName: 'Pool'},
      { title: 'Post Office', id: 21, imgName: 'PostOffice'},
      { title: 'Shop', id: 22, imgName: 'Shop'},
      { title: 'Smoking Zone', id: 23, imgName: 'SmokingZone'},
      { title: 'Theatre', id: 24, imgName: 'Theatre'},
      { title: 'Work', id: 25, imgName: 'Work'},
      { title: 'Car', id: 26, imgName: 'Car'},
      { title: 'Railway Station', id: 27, imgName: 'RailwayStation'},
      { title: 'Library', id: 28, imgName: 'Library'},
      { title: 'Home', id: 29, imgName: 'Home'},
      { title: 'Bike', id: 30, imgName: 'Bike'},
      { title: 'Other', id: 31, imgName: 'Other'}
    ];


    $scope.checkLocationType = function($locationShareBy) {
    
       if($locationShareBy === $scope.currentL){
          if (navigator.geolocation) navigator.geolocation.getCurrentPosition(onPositionUpdate);
 
          function onPositionUpdate(position) {
              $scope.pointInLists.latitude = position.coords.latitude;
              $scope.pointInLists.longitude = position.coords.longitude;
              var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + $scope.pointInLists.latitude + "," + $scope.pointInLists.longitude + "&sensor=true";
              $http.get(url).then(function(result) {
                      var address = result.data.results[2].formatted_address;
                      $scope.address = address;
                      console.log($scope.address);
                  });
          }

       }
       /*current location ends*/
       if($locationShareBy === $scope.searchL){
              var input = document.getElementById('pac-input');
              var searchBox = new google.maps.places.SearchBox(input);
              searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();
                console.log(places);
                $scope.pointInLists.latitude = places[0].geometry.location.lat();
                $scope.pointInLists.longitude = places[0].geometry.location.lng();
              });
  
       }
       /*search location :: ends*/
    }

    $scope.setlocationCat = function(pointInSelected){
      $scope.pointInLists.title = pointInSelected.title;
      console.log(pointInSelected.title);
    }

    function validatePointIn(){
      
       if($scope.pointInLists.description == "" && $scope.pointInLists.title == 'Select PointIn' ){
            alert("blanck");
            //return false;
        }
        else{
          //return false;
        }
    }

    function resetPointInFields() {
        $scope.pointInLists.description = "";
        $scope.pointInLists.title = "";
        document.getElementById("pac-input").value = "";
        //document.getElementsByTagName("option").setAttribute("selected", "selected");

    }
    $scope.savePointIn = function() {
      //validatePointIn();
      $scope.pointInObj = { 
         title: $scope.pointInLists.title,
         description: $scope.pointInLists.description,
         date: new Date(),
         latitude: $scope.pointInLists.latitude,
         longitude: $scope.pointInLists.longitude 
       };

          $scope.pointInLists.push($scope.pointInObj);
          localStorageService.set('pointInData', $scope.pointInLists);
          $scope.pointInObj = {};

      /*localStorage.pointInLists = JSON.stringify($scope.pointInLists);
       $scope.pointInLists = JSON.parse(localStorage.pointInLists);*/
        resetPointInFields();
        $state.transitionTo('app.pointInLists', null, {'reload':true});
        /*$state.go('app.pointInLists').then(function(){
          getPointIn();
        });*/
       //console.log(JSON.parse(localStorage["pointInLists"]));
    }
/* ================================== add Location:: ends ======================================*/
/* ================================== Point In :: starts ======================================*/
    function getPointIn() {
      //fetches task from local storage
          if (localStorageService.get('pointInData')) {
              $scope.pointInLists = localStorageService.get('pointInData');
          } else {
              $scope.pointInLists = [];
          }

        /*if(localStorage["pointInLists"] == null && localStorage["pointInLists"] == undefined) {
          $scope.pointInLists = [];
        }
        else {
          $scope.pointInLists = localStorage["pointInLists"];
        }*/
        //$scope.pointInLists = JSON.parse(localStorage["pointInLists"]);
        console.log($scope.pointInLists);
    } 
    $scope.deletePointIn = function(key) {
         var confirmPopup = $ionicPopup.confirm({
           title: 'Delete location',
           template: 'Do you really want to delete location?'
         });
         confirmPopup.then(function(res) {

           if(res) {
              $scope.pointInLists.splice(key, 1);
              localStorageService.set('pointInData', $scope.pointInLists);
            /*for ( var i = 0; i < $scope.pointInLists.length; i++ ) {
                  if ( $scope.pointInLists[i].id === pointInList.id ) {
                      $scope.pointInLists.splice(i,1); 
                  }
              }
              localStorage.pointInLists = JSON.stringify($scope.pointInLists);
             console.log('You are sure');*/
           } else {
             console.log('You are not sure');
           }
         });
         //$state.transitionTo('app.pointInLists', null, {'reload':true});
         getPointIn();
    }  

    function shareAsEvent() {
        $ionicModal.fromTemplateUrl('templates/shareAsEvent.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });
   
        $scope.openShareEvent = function() {
          $scope.modal.show();
        };
        $scope.closeShareEvent = function() {
          $scope.modal.hide();
        };     
    }
    function shareAsAddress() {
        $ionicModal.fromTemplateUrl('templates/shareAsAddress.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });
   
        $scope.openShareAddress = function() {
          $scope.modal.show();
        };
        $scope.closeShareAddress = function() {
          $scope.modal.hide();
        };     
    }
    function initModule() {
      //getPointIn();
      if (localStorageService.get('pointInData')) {
            $scope.pointInLists = localStorageService.get('pointInData');
        } else {
            $scope.pointInLists = [];
        }
      shareAsEvent();
      shareAsAddress();
      // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.modal.remove();
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function() {
          // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
          // Execute action
        });
    }
    initModule();

/* ================================== Point In:: ends ======================================*/
});
/*.controller('PointInListsCtrl', function($scope, $ionicModal, $ionicPopup, localStorageService) {
    function getPointInData() {
        if(localStorage["pointInLists"] == null && localStorage["pointInLists"] == undefined) {
          $scope.pointInLists = [];
        }
        else {
          $scope.pointInLists = localStorage["pointInLists"];
        }
        //$scope.pointInLists = JSON.parse(localStorage["pointInLists"]);
        console.log($scope.pointInLists);
      }    
    function shareAsEvent(){
        $ionicModal.fromTemplateUrl('templates/shareAsEvent.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });
   
        $scope.openShareEvent = function() {
          $scope.modal.show();
        };
        $scope.closeShareEvent = function() {
          $scope.modal.hide();
        };     
    }
    function shareAsAddress(){
        $ionicModal.fromTemplateUrl('templates/shareAsAddress.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.modal = modal;
        });
   
        $scope.openShareAddress = function() {
          $scope.modal.show();
        };
        $scope.closeShareAddress = function() {
          $scope.modal.hide();
        };     
    }
    $scope.deletePointIn = function(pointInList){
         var confirmPopup = $ionicPopup.confirm({
           title: 'Delete location',
           template: 'Do you really want to delete location?'
         });
         confirmPopup.then(function(res) {

           if(res) {
            for ( var i = 0; i < $scope.pointInLists.length; i++ ) {
                  if ( $scope.pointInLists[i].id === pointInList.id ) {
                      $scope.pointInLists.splice(i,1); 
                  }
              }
              localStorage.pointInLists = JSON.stringify($scope.pointInLists);
             console.log('You are sure');
           } else {
             console.log('You are not sure');
           }
         });
    }
    function initModule() {
      getPointInData();
      shareAsEvent();
      shareAsAddress();
      // Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function() {
          $scope.modal.remove();
        });
        // Execute action on hide modal
        $scope.$on('modal.hidden', function() {
          // Execute action
        });
        // Execute action on remove modal
        $scope.$on('modal.removed', function() {
          // Execute action
        });
    }
    initModule();
})*/
 
